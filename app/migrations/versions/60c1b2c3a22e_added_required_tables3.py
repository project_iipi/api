"""Added required tables3

Revision ID: 60c1b2c3a22e
Revises: 96be11228a65
Create Date: 2024-01-01 13:03:21.528411

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '60c1b2c3a22e'
down_revision: Union[str, None] = '96be11228a65'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('category_posts',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(length=100), nullable=True),
    sa.Column('slug', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_constraint('posts_category_post_id_fkey', 'posts', type_='foreignkey')
    op.create_foreign_key(None, 'posts', 'category_posts', ['category_post_id'], ['id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'posts', type_='foreignkey')
    op.create_foreign_key('posts_category_post_id_fkey', 'posts', 'posts', ['category_post_id'], ['id'])
    op.drop_table('category_posts')
    # ### end Alembic commands ###
