"""create fixed new

Revision ID: 504683992a4a
Revises: 0ca149719da5
Create Date: 2024-03-06 14:35:40.624993

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '504683992a4a'
down_revision: Union[str, None] = '0ca149719da5'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
