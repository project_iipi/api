import sqlalchemy
from pydantic import BaseModel

metadata = sqlalchemy.MetaData()

posts_table = sqlalchemy.Table(
    "professors",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("surname", sqlalchemy.String(100)),
    sqlalchemy.Column("name", sqlalchemy.String(200)),
    sqlalchemy.Column("middle_name", sqlalchemy.String(200)),
    sqlalchemy.Column("post", sqlalchemy.String(200)),
    sqlalchemy.Column("photo", sqlalchemy.String(200)),
)


class Professors(BaseModel):
    id: int
    surname: str
    name: str
    middle_name: str
    post: str
    photo: str
