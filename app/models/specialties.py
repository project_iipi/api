import sqlalchemy
from pydantic import BaseModel
from typing import Union

metadata = sqlalchemy.MetaData()

posts_table = sqlalchemy.Table(
    "specialties",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("title", sqlalchemy.String(100)),
    sqlalchemy.Column("direction", sqlalchemy.String(200)),
    sqlalchemy.Column("profile", sqlalchemy.String(200)),
    sqlalchemy.Column("period_development", sqlalchemy.String(200)),
    sqlalchemy.Column("form_education", sqlalchemy.String(200)),
    sqlalchemy.Column("is_dogovor", sqlalchemy.Boolean),
    sqlalchemy.Column("is_budget", sqlalchemy.Boolean),
)


class Specialties(BaseModel):
    id: int
    title: str
    direction: str
    profile: str
    period_development: str
    form_education: str
    is_dogovor: Union[bool, None] = None
    is_budget: Union[bool, None] = None
