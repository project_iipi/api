

import sqlalchemy
from datetime import datetime
from .users import users_table
from .category_posts import category_posts_table
from pydantic import BaseModel

metadata = sqlalchemy.MetaData()

posts_table = sqlalchemy.Table(
    "posts",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("user_id", sqlalchemy.ForeignKey(users_table.c.id)),
    sqlalchemy.Column("category_post_id", sqlalchemy.ForeignKey(category_posts_table.c.id)),
    sqlalchemy.Column("created_at", sqlalchemy.DateTime()),
    sqlalchemy.Column("title", sqlalchemy.String(100)),
    sqlalchemy.Column("content", sqlalchemy.Text()),
    sqlalchemy.Column("contentnext", sqlalchemy.Text()),
)


class Posts(BaseModel):
    id: int
    user_id: int
    category_post_id: int
    created_at: datetime
    title: str
    content: str
    contentnext: str
