import sqlalchemy

from pydantic import BaseModel

metadata = sqlalchemy.MetaData()


category_posts_table = sqlalchemy.Table(
    "category_posts",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("title", sqlalchemy.String(100)),
    sqlalchemy.Column("slug", sqlalchemy.String(100)),
)

class CategoryPosts(BaseModel):
    id: int
    title: str
    slug: str
