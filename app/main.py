from fastapi import FastAPI
from databases import Database
from typing import List
from fastapi.middleware.cors import CORSMiddleware
from .models.professors import Professors
from .models.specialties import Specialties
from .models.posts import Posts
from .models.category_posts import CategoryPosts
app = FastAPI()

origins = [
    "http://127.0.0.1",
    "http://127.0.0.1:3000",
    "http://localhost",
    "http://localhost:3000",
    "http://127.0.0.1:8237",
    "http://localhost:8237",
    "https://ipi.rfpgu.ru/",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

DATABASE_URL = "postgresql://postgres:postgres@db_api_iipi/apidb"

database = Database(DATABASE_URL)


@app.on_event("startup")
async def startup_database():
    await database.connect()


@app.on_event("shutdown")
async def shutdown_database():
    await database.disconnect()


@app.get("/")
async def root():
    return {"message": "Api version 1"}


@app.get("/v1/list_educations")
async def get_list_educations() -> List[Specialties]:
    query = "SELECT * FROM specialties"
    result = await database.fetch_all(query=query)
    resultArray = []
    for val in result:
        resultArray.append({"id": val['id'],
                            "title": val['title'],
                            "direction": val['direction'],
                            "profile": val['profile'],
                            "period_development": val['period_development'],
                            "form_education": val['form_education'],
                            "is_dogovor": val['is_dogovor'],
                            "is_budget": val['is_budget'],
                            })
    return resultArray


@app.get("/v1/teachers")
async def get_list_teacher() -> List[Professors]:
    query = "SELECT * FROM professors"
    result = await database.fetch_all(query=query)
    resultArray = []
    for val in result:
        resultArray.append({"id": val['id'],
                            "surname": val['surname'],
                            "name": val['name'],
                            "middle_name": val['middle_name'],
                            "post": val['post'],
                            "photo": val['photo'],
                            })
    return resultArray


@app.get("/v1/post/category_posts")
async def get_list_category_posts() -> List[CategoryPosts]:
    query = "SELECT * FROM category_posts"
    result = await database.fetch_all(query=query)
    resultArray = []
    for val in result:
        resultArray.append({"id": val['id'],
                            "title": val['title'],
                            "slug": val['slug'],
                            })
    return resultArray


@app.get("/v1/posts")
async def get_list_posts() -> List[Posts]:
    query = "SELECT * FROM posts order by created_at desc limit 3"
    result = await database.fetch_all(query=query)
    resultArray = []
    for val in result:
        resultArray.append({"id": val['id'],
                            "user_id": val['user_id'],
                            "category_post_id": val['category_post_id'],
                            "created_at": val['created_at'],
                            "title": val['title'],
                            "content": val['content'],
                            })
    return resultArray

